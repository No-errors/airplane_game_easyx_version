#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#include<graphics.h>
#include<conio.h>
#include<windows.h>
//游戏画面尺寸
#define High 650
#define Width 591

IMAGE img_bk;		//背景
IMAGE img_planeNormal1, img_planeNormal2;	//正常飞机图片
int position_x, position_y;		//飞机位置
 
void startup();
void show();
void UpdateWithoutInput();
void UpdateWithInput();



void HideCursor()
{
	CONSOLE_CURSOR_INFO cursor_info = { 1, 0 };
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursor_info);
}
/***********************************************************/
int main()
{
	startup();					//初始化变量 
	while (1) {
		show();					//显示画面 
		UpdateWithoutInput();   //与用户输入无关的变化 
		UpdateWithInput();      //与用户输入有关的变化 
	}
	return 0;
}
/*************************************************************/
void startup()				//初始化变量 
{
	initgraph(Width, High);
	loadimage(&img_bk, "C:\\plane\\飞机大战图片音乐素材\\background.jpg");
	loadimage(&img_planeNormal1, "C:\\plane\\飞机大战图片音乐素材\\planeNormal_1.jpg");
	loadimage(&img_planeNormal2, "C:\\plane\\飞机大战图片音乐素材\\planeNormal_2.jpg");
	loadimage(&img_bullet1, "C:\\plane\\飞机大战图片音乐素材\\bullet1.jpg");
	loadimage(&img_bullet2, "C:\\plane\\飞机大战图片音乐素材\\bullet2.jpg"); 
	position_x = High / 2;
	position_y = Width * 7 / 10;
	bullet_x = position_x - 7;
	bullet_y = -10; 

	BeginBatchDraw();
}
/***********************************************************/
void show()					//显示画面 
{
	putimage(0, 0, &img_bk);
	putimage(position_x - 50, position_y - 30, &img_planeNormal1, NOTSRCERASE);	//显示正常飞机
	putimage(position_x - 50, position_y - 30, &img_planeNormal2, SRCINVERT);
 
	FlushBatchDraw();
	Sleep(5);
}
/**************************************************************/
void UpdateWithoutInput()   //与用户输入无关的变化 
{
	if (bullet_y >= -50)
		bullet_y--;
	if (enemy_y < High)
		enemy_y++; 
}
/**************************************************************/
void UpdateWithInput()      //与用户输入有关的变化 
{
	MOUSEMSG m;		//记录鼠标消息
	while (MouseHit()) {
		m = GetMouseMsg();
		if (m.uMsg == WM_MOUSEMOVE) {		//鼠标移动时，飞机坐标等于鼠标位置
			position_x = m.x;
			position_y = m.y;
		} 
	}
}